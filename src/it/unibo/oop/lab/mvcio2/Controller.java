package it.unibo.oop.lab.mvcio2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Controller {
    
    static File current = new File(System.getProperty("user.home") + File.separator + "output.txt");

    public String getPath() throws IOException {
        return current.getCanonicalPath();
    }
    
    public File getCurrent() {
        return current;
    }

    public static void setCurrent(File current) {
        Controller.current = current;
    }
    
    static public void setString(String input) throws IOException{
        FileOutputStream outputStream = new FileOutputStream(current);
        byte[] toWrite = input.getBytes();
        outputStream.write(toWrite);
        outputStream.close();
    }
        
}
