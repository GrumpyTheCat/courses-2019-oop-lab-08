package it.unibo.oop.lab.mvcio2;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import it.unibo.oop.lab.mvcio.SimpleGUI;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
    
    private final JFrame frame = new JFrame();
    private static final int PROPORTION = 5;

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

    public SimpleGUIWithFileChooser() {
        JPanel myPanel = new JPanel();
        JPanel mySecondPanel = new JPanel();
        myPanel.setLayout(new BorderLayout());
        mySecondPanel.setLayout(new BorderLayout());
        JButton BTNBrowse = new JButton("Browse...");
        JTextField TXTSelectedFile = new JTextField();
        myPanel.add(mySecondPanel, BorderLayout.NORTH);
        mySecondPanel.add(TXTSelectedFile, BorderLayout.CENTER);
        mySecondPanel.add(BTNBrowse, BorderLayout.LINE_END);
        TXTSelectedFile.setEnabled(false);
        
        BTNBrowse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                JFileChooser fileToChoose = new JFileChooser();
                int n = fileToChoose.showSaveDialog(TXTSelectedFile);
                if (n == JFileChooser.APPROVE_OPTION)
                    Controller.setCurrent(fileToChoose.getSelectedFile());
                else if(n == JFileChooser.CANCEL_OPTION) {  }
                else
                    JOptionPane.showMessageDialog(TXTSelectedFile, "An error occurred during file selection");
                   
                TXTSelectedFile.setText(fileToChoose.getSelectedFile().getName());
            }
            
        });
       
        frame.setContentPane(myPanel);
    }
    
    private void display() {
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        
        frame.setSize(sw / PROPORTION, sh / PROPORTION);
        frame.setLocationByPlatform(true);
        frame.pack();
        frame.setVisible(true);

    }
    
    public static void main(final String... args) {
        new SimpleGUIWithFileChooser().display();
    }

    
}
